package com.petz.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petz.dto.ClienteDTO;
import com.petz.model.Cliente;
import com.petz.service.ClienteService;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteService service;
	
	@GetMapping()
	public ResponseEntity< List<ClienteDTO> > get() {
		return ResponseEntity.ok( service.get() );
	}
	
	@GetMapping("/find/{cpf}")
	public ResponseEntity<Optional<ClienteDTO> > get(@PathVariable("cpf") String cpf) {
		return ResponseEntity.ok( service.findByCpf(cpf) );
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<ClienteDTO> find(@PathVariable("id") String id) {
		return ResponseEntity.ok( service.findById(Long.parseLong(id)) );
	}
	
	@PutMapping()
	public ResponseEntity<Optional<Cliente>> put(@RequestBody() Cliente cliente) {
		return ResponseEntity.ok( service.put(cliente) );
	}
	
	@DeleteMapping("/{id}")
	public String delete(@PathVariable("id") Long id) {
		
		service.delete(id);
		
		return "Carro deletado com sucesso.";
		
	}

}
