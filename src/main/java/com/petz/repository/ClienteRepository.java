package com.petz.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.petz.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	
	public Optional<Cliente> findByCpf(String cpf);
	
	public Optional<Cliente> findById(Long id);
}
