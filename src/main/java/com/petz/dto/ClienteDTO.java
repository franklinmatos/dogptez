package com.petz.dto;

import java.util.List;

import com.petz.model.Cliente;
import com.petz.model.Pet;

import lombok.Data;

@Data
public class ClienteDTO {
	
	private Long id;
	private String nome;
	private String cpf;
	private String celular;
	private List<Pet> pets;
	
	
	public ClienteDTO(Cliente cliente) {
		this.id = cliente.getId();
		this.nome = cliente.getNome();
		this.cpf = cliente.getCpf();
		this.celular = cliente.getCelular();
		this.pets = cliente.getPets();
	}

}
