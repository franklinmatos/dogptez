package com.petz.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	private Date dtNascimento;
	private String cpf;
	private String telefone;
	private String celular;
	private String rua;
	private String complemento;
	private int numero;
	private String Bairro;
	
	@OneToMany()
	private List<Pet> pets;
	

}
