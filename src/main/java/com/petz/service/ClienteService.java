package com.petz.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.petz.dto.ClienteDTO;
import com.petz.model.Cliente;
import com.petz.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository repository;

	public List<ClienteDTO> get() {
		return repository.findAll().stream().map(ClienteDTO::new).collect(Collectors.toList());
	}

	public Optional<ClienteDTO> findByCpf(String cpf) {
		return repository.findByCpf(cpf).map(ClienteDTO::new);
	}

	public ClienteDTO findById(Long id) {
		Optional<Cliente> cliente = repository.findById(id);
		ClienteDTO op = new ClienteDTO(cliente.get());
		return op;
	}

	public void post(Cliente cliente) {
		repository.saveAndFlush(cliente);
	}

	public Optional<Cliente> put(Cliente cliente) {
		Assert.isNull(cliente.getId(), "Não foi possível atualizar o registro.");

		Optional<Cliente> op = repository.findById(cliente.getId());
		if( op.isPresent() ) {
			return Optional.ofNullable(repository.saveAndFlush(cliente));	
		}else {
			throw new RuntimeException("Não foi possível encontrar o cliente para atualizar.");
		}
		
	}

	public void delete(Long id) {
		Assert.isNull(id, "Não foi possível apagar o registro.");

		Optional<Cliente> op = repository.findById(id);

		if (op.isPresent()) {
			repository.delete(op.get());
		} else {
			throw new RuntimeException("Não foi possível encontrar o cliente para ser apagado.");
		}
	}

	public ClienteDTO fromDTO(Cliente cliente) {

		return new ClienteDTO(cliente);

	}
}
